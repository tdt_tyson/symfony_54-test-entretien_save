<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;
use function PHPUnit\Framework\throwException;


class ArticleController extends AbstractController
{

    #[Route('/articles', name: 'articles')]
    public function index(ManagerRegistry $registry): Response
    {
        $em = $registry->getManager();
        $articles = $em->getRepository(Article::class)->findAll();

        return $this->render('article/articles.html.twig',[
            'articles' => $articles
        ]);
    }

    #[Route('/articles/add', name: 'add_articles')]
    public function addArticle(ManagerRegistry $registry, Request $request): Response
    {
        $em = $registry->getManager();
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $article = $form->getData();

            $slugger = new AsciiSlugger();
            $slug = $slugger->slug($form->get('title')->getData());
            $article->setSlug($slug);
            $photo = $form->get("photoName")->getData();

            if($photo){
                $originalFilename = pathinfo($photo->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$photo->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $photo->move(
                        $this->getParameter('photo_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                $article->setPhoto($newFilename);
            }

            $em->persist($article);
            $em->flush();

            return $this->redirect($this->generateUrl('articles'));
        }

        return $this->render('article/newArticle.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    #[Route('/articles/{slug}', name: 'get_article')]
    public function getArticleBySlug(ManagerRegistry $registry, string $slug): Response
    {
        $em = $registry->getManager();
        $repository = $em->getRepository(Article::class);
        $articles = $repository->findAll();
        $article = $repository->findOneBy([
            'slug' => $slug,
        ]);

        return $this->render('article/article.html.twig',[
            'article' => $article,
            'articles' => $articles,
        ]);
    }

    #[Route('/articles/update/{slug}', name: 'update_article')]
    public function updateArticleBySlug(ManagerRegistry $registry,string $slug, Request $request): Response
    {
        $em = $registry->getManager();
        $article = $em->getRepository(Article::class)->findOneBy([
            'slug' => $slug,
        ]);
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $article = $form->getData();
            $em->persist($article);
            $em->flush();

            return $this->redirect($this->generateUrl('articles'));
        }

        return $this->render('article/updateArticle.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    #[Route('/articles/delete/{slug}', name: 'delete_article')]
    public function deleteArticleBySlug(ManagerRegistry $registry, string $slug): Response
    {
        $em = $registry->getManager();
        $article = $em->getRepository(Article::class)->findOneBy([
            'slug' => $slug,
        ]);

        $em->remove($article);
        $em->flush();

        return $this->redirect($this->generateUrl('articles'));
    }

    //REST CRUD Methods
    #[Route('/articles/create', name: 'create_article', methods: 'POST')]
    public function createArticle(ManagerRegistry $registry, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $slugger = new AsciiSlugger();

        $title = $data['title'];
        $slug = $slugger->slug($title);
        $introduction = $data['introduction'];
        $content = $data['content'];

        //I don't know how to handle the pictures..

        if(empty($title) || empty($content))
            throw new NotFoundHttpException('Expecting mandatory parameters!');

        $article = new Article();
        $article->setTitle($title);
        $article->setSlug($slug);
        $article->setIntroduction($introduction);
        $article->setContent($content);

        $registry->getManager()->persist($article);
        $registry->getManager()->flush();

        return new JsonResponse(['status' => 'Article created!'], Response::HTTP_CREATED);
    }

    #[Route('/articles/{id}', name: 'get_article', methods: 'GET')]
    public function getArticle(ManagerRegistry $registry,int $id): JsonResponse
    {
        /**
         * @var $article Article
         */
        $article = $registry->getManager()->getRepository(Article::class)->findOneBy(array('id' => $id));

        if(empty($article))
            throw new NotFoundHttpException('No article found!');

        $data = [
            'id' => $article->getId(),
            'title' => $article->getTitle(),
            'slug' => $article->getSlug(),
            'introduction' => $article->getIntroduction(),
            'content' => $article->getContent(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    #[Route('/articles/{id}', name: 'update_article', methods: 'PUT')]
    public function updateArticle(ManagerRegistry $registry,int $id, Request $request): JsonResponse
    {
        /**
         * @var $article Article
         */
        $article = $registry->getManager()->getRepository(Article::class)->findOneBy(array('id' => $id));
        $data = json_decode($request->getContent(), true);

        if(empty($article))
            throw new NotFoundHttpException('No article found!');

        empty($data['content']) ? : $article->setContent($data['content']);
        empty($data['introduction']) ? : $article->setIntroduction($data['introduction']);

        $registry->getManager()->persist($article);
        $registry->getManager()->flush();

        return new JsonResponse(['status' => 'Article updated!'], Response::HTTP_OK);
    }

    #[Route('/articles/{id}', name: 'delete_article', methods: 'DELETE')]
    public function deleteArticle(ManagerRegistry $registry,int $id): JsonResponse
    {
        /**
         * @var $article Article
         */
        $article = $registry->getManager()->getRepository(Article::class)->findOneBy(array('id' => $id));

        $registry->getManager()->remove($article);
        $registry->getManager()->flush();

        return new JsonResponse(['status' => 'Article deleted!'], Response::HTTP_NO_CONTENT);
    }

}
