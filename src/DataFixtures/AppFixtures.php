<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\AsciiSlugger;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $slugger = new AsciiSlugger();

        $article = new Article();
        $article->setTitle("Article 1");
        $article->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae ante a velit molestie mattis eget nec dui. Duis at efficitur neque. Praesent vitae euismod nulla. Phasellus dignissim nisl vitae risus ornare lacinia. Phasellus semper elit at magna condimentum, a euismod lorem varius. Duis vel nibh sed massa tempor lobortis at at sem. Aliquam commodo ex eget ullamcorper vehicula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras non mi ac risus accumsan posuere. Nullam id aliquet lectus. Cras tellus diam, semper et placerat sed, maximus vitae eros. Nulla convallis, eros eu suscipit laoreet, leo tellus viverra turpis, convallis imperdiet augue sapien sed turpis. Maecenas augue leo, viverra vitae pellentesque vel, consectetur vel sapien. Cras massa risus, vestibulum quis dolor at, sodales sagittis magna. Phasellus hendrerit at mi eu porta. Pellentesque eleifend, libero varius facilisis feugiat, lorem lacus pharetra est, a dapibus velit risus quis libero.

Cras malesuada consectetur purus, id cursus mauris feugiat et. Donec accumsan turpis at faucibus hendrerit. Praesent nulla est, fermentum eu vehicula in, gravida sed arcu. Pellentesque id mauris mi. Fusce ullamcorper euismod nisl, ut tincidunt elit fringilla sed. Praesent efficitur ornare molestie. Praesent eget arcu tempor, tincidunt quam ut, commodo ligula. Phasellus at leo vitae elit tincidunt blandit fringilla quis nisl. Vivamus blandit, nunc ac sollicitudin cursus, dolor metus lacinia enim, eu posuere est est eget est.

Nulla quis luctus tellus. Mauris vitae massa lobortis, gravida leo a, accumsan quam. Pellentesque pharetra mi justo, vitae iaculis ligula egestas ut. Vivamus ut eros tristique, condimentum eros id, fermentum magna. Nunc non ullamcorper elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In hac habitasse platea dictumst. Nam convallis scelerisque lorem porttitor ultrices.

Proin velit leo, posuere eu commodo sit amet, fermentum et purus. Vestibulum sed nulla ac nulla porttitor consectetur non eu nisi. Nulla a augue eget nibh pretium lobortis sit amet sit amet purus. Vestibulum aliquet, ipsum quis porttitor consectetur, arcu tellus dignissim quam, eu volutpat lacus tellus ut ex. Vivamus sem tortor, rutrum sit amet elit in, scelerisque ullamcorper erat. Mauris vitae efficitur neque. Praesent efficitur, magna ut euismod vulputate, ante augue rhoncus lacus, vitae convallis metus velit a magna. Donec pellentesque vitae neque in hendrerit. Duis magna lectus, aliquet tincidunt faucibus id, pellentesque sit amet dui. Praesent blandit, mauris malesuada lacinia laoreet, nisi ante elementum nibh, sed facilisis enim risus ut ante. Donec ac erat et leo suscipit tempor eget sit amet eros. Maecenas posuere hendrerit ante eu tristique. Nam vel sem sem. Nullam consequat aliquam lacus sed consectetur. Pellentesque tempor elit ut justo sollicitudin hendrerit.

Integer blandit nisl eleifend enim mollis pharetra. In vitae ipsum velit. Aenean convallis tristique fermentum. Phasellus velit arcu, faucibus a pellentesque mattis, rutrum eu leo. Praesent eu tincidunt diam. Nam eleifend enim ac neque mattis blandit in et lorem. Aliquam consequat feugiat mauris, ut porta purus condimentum vel.");

        $article->setPhoto("186-600x300.jpg");
        $slug = $slugger->slug($article->getTitle());
        $article->setSlug($slug);

        $article2 = new Article();
        $article2->setTitle("Article 2");
        $article2->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae ante a velit molestie mattis eget nec dui. Duis at efficitur neque. Praesent vitae euismod nulla. Phasellus dignissim nisl vitae risus ornare lacinia. Phasellus semper elit at magna condimentum, a euismod lorem varius. Duis vel nibh sed massa tempor lobortis at at sem. Aliquam commodo ex eget ullamcorper vehicula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras non mi ac risus accumsan posuere. Nullam id aliquet lectus. Cras tellus diam, semper et placerat sed, maximus vitae eros. Nulla convallis, eros eu suscipit laoreet, leo tellus viverra turpis, convallis imperdiet augue sapien sed turpis. Maecenas augue leo, viverra vitae pellentesque vel, consectetur vel sapien. Cras massa risus, vestibulum quis dolor at, sodales sagittis magna. Phasellus hendrerit at mi eu porta. Pellentesque eleifend, libero varius facilisis feugiat, lorem lacus pharetra est, a dapibus velit risus quis libero.

Cras malesuada consectetur purus, id cursus mauris feugiat et. Donec accumsan turpis at faucibus hendrerit. Praesent nulla est, fermentum eu vehicula in, gravida sed arcu. Pellentesque id mauris mi. Fusce ullamcorper euismod nisl, ut tincidunt elit fringilla sed. Praesent efficitur ornare molestie. Praesent eget arcu tempor, tincidunt quam ut, commodo ligula. Phasellus at leo vitae elit tincidunt blandit fringilla quis nisl. Vivamus blandit, nunc ac sollicitudin cursus, dolor metus lacinia enim, eu posuere est est eget est.

Nulla quis luctus tellus. Mauris vitae massa lobortis, gravida leo a, accumsan quam. Pellentesque pharetra mi justo, vitae iaculis ligula egestas ut. Vivamus ut eros tristique, condimentum eros id, fermentum magna. Nunc non ullamcorper elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In hac habitasse platea dictumst. Nam convallis scelerisque lorem porttitor ultrices.

Proin velit leo, posuere eu commodo sit amet, fermentum et purus. Vestibulum sed nulla ac nulla porttitor consectetur non eu nisi. Nulla a augue eget nibh pretium lobortis sit amet sit amet purus. Vestibulum aliquet, ipsum quis porttitor consectetur, arcu tellus dignissim quam, eu volutpat lacus tellus ut ex. Vivamus sem tortor, rutrum sit amet elit in, scelerisque ullamcorper erat. Mauris vitae efficitur neque. Praesent efficitur, magna ut euismod vulputate, ante augue rhoncus lacus, vitae convallis metus velit a magna. Donec pellentesque vitae neque in hendrerit. Duis magna lectus, aliquet tincidunt faucibus id, pellentesque sit amet dui. Praesent blandit, mauris malesuada lacinia laoreet, nisi ante elementum nibh, sed facilisis enim risus ut ante. Donec ac erat et leo suscipit tempor eget sit amet eros. Maecenas posuere hendrerit ante eu tristique. Nam vel sem sem. Nullam consequat aliquam lacus sed consectetur. Pellentesque tempor elit ut justo sollicitudin hendrerit.

Integer blandit nisl eleifend enim mollis pharetra. In vitae ipsum velit. Aenean convallis tristique fermentum. Phasellus velit arcu, faucibus a pellentesque mattis, rutrum eu leo. Praesent eu tincidunt diam. Nam eleifend enim ac neque mattis blandit in et lorem. Aliquam consequat feugiat mauris, ut porta purus condimentum vel.");
        $article2->setPhoto("483-600x300.jpg");
        $slug = $slugger->slug($article2->getTitle());
        $article2->setSlug($slug);

        $article3 = new Article();
        $article3->setTitle("Article 3");
        $article3->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae ante a velit molestie mattis eget nec dui. Duis at efficitur neque. Praesent vitae euismod nulla. Phasellus dignissim nisl vitae risus ornare lacinia. Phasellus semper elit at magna condimentum, a euismod lorem varius. Duis vel nibh sed massa tempor lobortis at at sem. Aliquam commodo ex eget ullamcorper vehicula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras non mi ac risus accumsan posuere. Nullam id aliquet lectus. Cras tellus diam, semper et placerat sed, maximus vitae eros. Nulla convallis, eros eu suscipit laoreet, leo tellus viverra turpis, convallis imperdiet augue sapien sed turpis. Maecenas augue leo, viverra vitae pellentesque vel, consectetur vel sapien. Cras massa risus, vestibulum quis dolor at, sodales sagittis magna. Phasellus hendrerit at mi eu porta. Pellentesque eleifend, libero varius facilisis feugiat, lorem lacus pharetra est, a dapibus velit risus quis libero.

Cras malesuada consectetur purus, id cursus mauris feugiat et. Donec accumsan turpis at faucibus hendrerit. Praesent nulla est, fermentum eu vehicula in, gravida sed arcu. Pellentesque id mauris mi. Fusce ullamcorper euismod nisl, ut tincidunt elit fringilla sed. Praesent efficitur ornare molestie. Praesent eget arcu tempor, tincidunt quam ut, commodo ligula. Phasellus at leo vitae elit tincidunt blandit fringilla quis nisl. Vivamus blandit, nunc ac sollicitudin cursus, dolor metus lacinia enim, eu posuere est est eget est.

Nulla quis luctus tellus. Mauris vitae massa lobortis, gravida leo a, accumsan quam. Pellentesque pharetra mi justo, vitae iaculis ligula egestas ut. Vivamus ut eros tristique, condimentum eros id, fermentum magna. Nunc non ullamcorper elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In hac habitasse platea dictumst. Nam convallis scelerisque lorem porttitor ultrices.

Proin velit leo, posuere eu commodo sit amet, fermentum et purus. Vestibulum sed nulla ac nulla porttitor consectetur non eu nisi. Nulla a augue eget nibh pretium lobortis sit amet sit amet purus. Vestibulum aliquet, ipsum quis porttitor consectetur, arcu tellus dignissim quam, eu volutpat lacus tellus ut ex. Vivamus sem tortor, rutrum sit amet elit in, scelerisque ullamcorper erat. Mauris vitae efficitur neque. Praesent efficitur, magna ut euismod vulputate, ante augue rhoncus lacus, vitae convallis metus velit a magna. Donec pellentesque vitae neque in hendrerit. Duis magna lectus, aliquet tincidunt faucibus id, pellentesque sit amet dui. Praesent blandit, mauris malesuada lacinia laoreet, nisi ante elementum nibh, sed facilisis enim risus ut ante. Donec ac erat et leo suscipit tempor eget sit amet eros. Maecenas posuere hendrerit ante eu tristique. Nam vel sem sem. Nullam consequat aliquam lacus sed consectetur. Pellentesque tempor elit ut justo sollicitudin hendrerit.

Integer blandit nisl eleifend enim mollis pharetra. In vitae ipsum velit. Aenean convallis tristique fermentum. Phasellus velit arcu, faucibus a pellentesque mattis, rutrum eu leo. Praesent eu tincidunt diam. Nam eleifend enim ac neque mattis blandit in et lorem. Aliquam consequat feugiat mauris, ut porta purus condimentum vel.");
        $slug = $slugger->slug($article3->getTitle());
        $article3->setSlug($slug);

        $article4 = new Article();
        $article4->setTitle("Article 4");
        $article4->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae ante a velit molestie mattis eget nec dui. Duis at efficitur neque. Praesent vitae euismod nulla. Phasellus dignissim nisl vitae risus ornare lacinia. Phasellus semper elit at magna condimentum, a euismod lorem varius. Duis vel nibh sed massa tempor lobortis at at sem. Aliquam commodo ex eget ullamcorper vehicula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras non mi ac risus accumsan posuere. Nullam id aliquet lectus. Cras tellus diam, semper et placerat sed, maximus vitae eros. Nulla convallis, eros eu suscipit laoreet, leo tellus viverra turpis, convallis imperdiet augue sapien sed turpis. Maecenas augue leo, viverra vitae pellentesque vel, consectetur vel sapien. Cras massa risus, vestibulum quis dolor at, sodales sagittis magna. Phasellus hendrerit at mi eu porta. Pellentesque eleifend, libero varius facilisis feugiat, lorem lacus pharetra est, a dapibus velit risus quis libero.

Cras malesuada consectetur purus, id cursus mauris feugiat et. Donec accumsan turpis at faucibus hendrerit. Praesent nulla est, fermentum eu vehicula in, gravida sed arcu. Pellentesque id mauris mi. Fusce ullamcorper euismod nisl, ut tincidunt elit fringilla sed. Praesent efficitur ornare molestie. Praesent eget arcu tempor, tincidunt quam ut, commodo ligula. Phasellus at leo vitae elit tincidunt blandit fringilla quis nisl. Vivamus blandit, nunc ac sollicitudin cursus, dolor metus lacinia enim, eu posuere est est eget est.

Nulla quis luctus tellus. Mauris vitae massa lobortis, gravida leo a, accumsan quam. Pellentesque pharetra mi justo, vitae iaculis ligula egestas ut. Vivamus ut eros tristique, condimentum eros id, fermentum magna. Nunc non ullamcorper elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In hac habitasse platea dictumst. Nam convallis scelerisque lorem porttitor ultrices.

Proin velit leo, posuere eu commodo sit amet, fermentum et purus. Vestibulum sed nulla ac nulla porttitor consectetur non eu nisi. Nulla a augue eget nibh pretium lobortis sit amet sit amet purus. Vestibulum aliquet, ipsum quis porttitor consectetur, arcu tellus dignissim quam, eu volutpat lacus tellus ut ex. Vivamus sem tortor, rutrum sit amet elit in, scelerisque ullamcorper erat. Mauris vitae efficitur neque. Praesent efficitur, magna ut euismod vulputate, ante augue rhoncus lacus, vitae convallis metus velit a magna. Donec pellentesque vitae neque in hendrerit. Duis magna lectus, aliquet tincidunt faucibus id, pellentesque sit amet dui. Praesent blandit, mauris malesuada lacinia laoreet, nisi ante elementum nibh, sed facilisis enim risus ut ante. Donec ac erat et leo suscipit tempor eget sit amet eros. Maecenas posuere hendrerit ante eu tristique. Nam vel sem sem. Nullam consequat aliquam lacus sed consectetur. Pellentesque tempor elit ut justo sollicitudin hendrerit.

Integer blandit nisl eleifend enim mollis pharetra. In vitae ipsum velit. Aenean convallis tristique fermentum. Phasellus velit arcu, faucibus a pellentesque mattis, rutrum eu leo. Praesent eu tincidunt diam. Nam eleifend enim ac neque mattis blandit in et lorem. Aliquam consequat feugiat mauris, ut porta purus condimentum vel.");
        $article4->setPhoto("678-600x300.jpg");
        $slug = $slugger->slug($article4->getTitle());
        $article4->setSlug($slug);

        $manager->persist($article);
        $manager->persist($article2);
        $manager->persist($article3);
        $manager->persist($article4);

        $manager->flush();
    }
}
